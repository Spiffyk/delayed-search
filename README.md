# Delayed Guess The Number

This is an example Kotlin implementation for a delayed Guess The Number as specified by the [Internet
Problem Solving Contest 2004 Problem D](https://ipsc.ksp.sk/2004/real/problems/d.html) and the corresponding
[solution](https://ipsc.ksp.sk/2004/real/solutions/d.html).
