package cz.spiffyk.meatmincer

import cz.spiffyk.meatmincer.guesser.*
import cz.spiffyk.meatmincer.thinker.Thinker
import cz.spiffyk.meatmincer.thinker.ThinkingMeatmincer
import kotlin.IllegalStateException
import kotlin.system.exitProcess

const val MIN = 1
const val MAX = 20
const val DELAY = 1

/**
 * Map of guessers by name.
 */
val GUESSER_MAP: Map<String, ((Thinker) -> Guesser)?> = mapOf(
        Pair("stdin", ::StdinGuesser),
        Pair("repeater-bin", ::RepeaterBinaryGuesser),
        Pair("random-bin", when (DELAY) {
            1 -> ::RandomBinaryGuesser
            else -> null
        }),
        Pair("ipsc", when (DELAY) {
            1 -> ::IpscDelay1Guesser
            else -> null
        }))

/**
 * Guess the number entry point.
 */
fun main(args: Array<String>) {

    val guesserName: String =
            if (args.isEmpty())
                GUESSER_MAP.keys.first()
            else
                args[0].toLowerCase()

    if (!GUESSER_MAP.containsKey(guesserName)) {
        System.err.println("No guesser with name '$guesserName' exists!")
        System.err.println("Available guessers: ${GUESSER_MAP.keys.joinToString(", ")}")
        System.err.println("Omit guesser completely to pick '${GUESSER_MAP.keys.first()}'")
        exitProcess(1)
    }

    val thinker =
            if (args.size >= 2)
                ThinkingMeatmincer(DELAY, MIN, MAX, args[1].toInt())
            else
                ThinkingMeatmincer(DELAY, MIN, MAX)

    val guesserFun = GUESSER_MAP[guesserName]
    if (guesserFun === null) {
        throw IllegalStateException("Guesser '$guesserName' not available for the current configuration!")
    }
    val guesser: Guesser = guesserFun.invoke(thinker)


    println("Thinker is thinking a number between $MIN and $MAX (${thinker.number})\n")
    guesser.guess()
    println("Done.")
}
