package cz.spiffyk.meatmincer

/**
 * A basic inclusive "interval" of integers.
 */
data class Interval(val min: Int, val max: Int) {

    /**
     * The number of individual integers contained by this interval.
     */
    val size = Math.max(0, max - min + 1)



    override fun toString(): String {
        return "<$min, $max> ($size)"
    }
}