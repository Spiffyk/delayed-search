package cz.spiffyk.meatmincer.thinker

/**
 * An interface for the thinker in the Guess The Number game.
 */
interface Thinker {

    /**
     * Flag of whether this thinker has been solved
     */
    val isSolved: Boolean

    /**
     * The maximum number of the played interval
     */
    val thinkMax: Int

    /**
     * The minimum number of the played interval
     */
    val thinkMin: Int

    /**
     * Performs one turn of the Guess The Number game.
     */
    fun guess(guess: Int): Result?


    /**
     * The result of one turn.
     *
     * @param guess the guessed value to which this result is an answer
     * @param value the actual result
     */
    data class Result(val guess: Int, val value: ResultValue)

    /**
     * The result value of one turn.
     */
    enum class ResultValue {

        /**
         * The **guessed** value is smaller than the thought value.
         */
        SMALLER,

        /**
         * The **guessed** value is equal to the thought value.
         */
        CORRECT,

        /**
         * The **guessed** value is greater than the thought value.
         */
        GREATER

    }

}