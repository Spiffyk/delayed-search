package cz.spiffyk.meatmincer.thinker

import java.util.*

/**
 * A delayed Guess The Number thinker implementation.
 *
 * @param delay the number of turns that the meatmincer ignores
 * @param number the number the meatmincer thinks
 */
class ThinkingMeatmincer(val delay: Int,
                         override val thinkMin: Int = 1,
                         override val thinkMax: Int = 20,
                         val number: Int = thinkMin + Random().nextInt(thinkMax - thinkMin + 1)) : Thinker {

    var turn = 0

    init {
        assert(number >= 1) { "Number ($number) is smaller than 1" }
        assert(number <= thinkMax) { "Number ($number) is greater than thinkMax ($thinkMax)" }
    }


    /**
     * The buffer guess buffer
     */
    private val buffer: Queue<Int> = LinkedList()


    override var isSolved: Boolean = false
        private set


    override fun guess(guess: Int): Thinker.Result? {
        if (isSolved) {
            println("${javaClass.simpleName} is already solved...")
            return null
        }

        turn++
        print("$turn) ")

        val size = buffer.size
        buffer.add(guess)
        var result: Thinker.Result? = null

        if (size < delay) {
            println("Thinker is silent...")
            result = null
        } else {
            val answeredGuess = buffer.poll()
            when {
                (answeredGuess == number) -> {
                    isSolved = true
                    result = Thinker.Result(answeredGuess, Thinker.ResultValue.CORRECT)
                    println("Thinker says ${result.guess} is ${result.value.name}!")
                }
                (answeredGuess < number) -> {
                    result = Thinker.Result(answeredGuess, Thinker.ResultValue.SMALLER)
                    println("Thinker says ${result.guess} is ${result.value.name} than its number.")
                }
                (answeredGuess > number) -> {
                    result = Thinker.Result(answeredGuess, Thinker.ResultValue.GREATER)
                    println("Thinker says ${result.guess} is ${result.value.name} than its number.")
                }
                else -> println("${javaClass.simpleName} is silent...")
            }
        }
        println()

        return result
    }
}
