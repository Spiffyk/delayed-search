package cz.spiffyk.meatmincer

object Util {

    /**
     * Calculates the middle value of an interval
     */
    fun calcMid(min: Int, max: Int): Int = (min + max) / 2

}