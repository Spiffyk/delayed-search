package cz.spiffyk.meatmincer.guesser

import cz.spiffyk.meatmincer.Interval
import cz.spiffyk.meatmincer.thinker.Thinker
import java.lang.IllegalStateException

/**
 * This guesser plays the delayed guessing game using the strategy described as a solution to the Internet Problem
 * Solving Contest 2004.
 */
class IpscDelay1Guesser(override val thinker: Thinker) : Guesser {

    private val b: MutableMap<Int, Int> = HashMap()
    private val x: Int

    init {
        b[0] = 0
        b[1] = 0
        b[2] = 1

        var x = 2
        var lastB: Int
        do {
            x++
            lastB = b[x - 1]!! + b[x - 2]!! + 1
            b[x] = lastB
        } while (lastB < thinker.thinkMax)
        this.x = x
    }



    override fun guess() {
        var x = this.x
        var i = Interval(1, b[x]!!)

        step2@ while(true) {
            val p = i.min + b[x - 1]!!
            var i1 = Interval(i.min, p - 1)
            var i2 = Interval(p + 1, i.max)

            println("Guessing $p...")
            thinker.guess(p)

            step3@ while(true) {
                val q = i1.min + b[x - 2]!!

                println("Guessing $q...")
                val result = thinker.guess(q)!!

                if (result.value == Thinker.ResultValue.CORRECT)
                    return

                when (result.value) {
                    Thinker.ResultValue.GREATER -> {
                        i1 = Interval(i1.min, q - 1)
                        i2 = Interval(q + 1, i1.max)
                        x -= 1
                        continue@step3 // not needed but kept for readability, don't you dare get clever with me!
                    }
                    Thinker.ResultValue.SMALLER -> {
                        i = i2
                        x -= 2
                        continue@step2
                    }
                    else -> throw IllegalStateException("This shouldn't happen!")
                }
            }
        }
    }

}