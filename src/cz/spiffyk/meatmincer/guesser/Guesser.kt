package cz.spiffyk.meatmincer.guesser

import cz.spiffyk.meatmincer.thinker.Thinker

/**
 * A guesser interface for playing Guess The Number.
 */
interface Guesser {

    /**
     * The thinker to play the guessing game with.
     */
    val thinker: Thinker

    /**
     * Plays the game with the thinker and stops once the thinker is solved.
     */
    fun guess()

}