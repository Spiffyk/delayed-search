package cz.spiffyk.meatmincer.guesser

import cz.spiffyk.meatmincer.Util
import cz.spiffyk.meatmincer.thinker.Thinker

/**
 * This guesser plays the delayed guessing game using the number repeating strategy. The guess is repeated until
 * the thinker returns its corresponding result.
 */
class RepeaterBinaryGuesser(override val thinker: Thinker) : Guesser {



    override fun guess() {
        var max = thinker.thinkMax
        var min = thinker.thinkMin

        loop@ while (!thinker.isSolved) {
            val mid = Util.calcMid(min, max);
            println("Guessing $mid...")

            val result = thinker.guess(mid)

            if (result !== null && result.guess == mid) {
                when (result.value) {
                    Thinker.ResultValue.GREATER -> max = mid // my guess was greater than the thought number
                    Thinker.ResultValue.SMALLER -> min = mid // my guess was smaller than the thought number
                    else -> continue@loop
                }
            }
        }
    }
}