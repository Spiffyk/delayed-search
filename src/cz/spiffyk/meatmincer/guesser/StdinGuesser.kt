package cz.spiffyk.meatmincer.guesser

import cz.spiffyk.meatmincer.thinker.Thinker

class StdinGuesser(override val thinker: Thinker) : Guesser {

    override fun guess() {
        while (!thinker.isSolved) {
            var input: Int?
            do {
                print("Specify guess: ")
                input = readLine()?.toIntOrNull()
                if (input === null) {
                    println("Not a number, try again.")
                }
            } while (input === null)

            thinker.guess(input)
        }
    }
}