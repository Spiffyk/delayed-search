package cz.spiffyk.meatmincer.guesser

import cz.spiffyk.meatmincer.Util
import cz.spiffyk.meatmincer.thinker.Thinker
import java.util.*

/**
 * This guesser plays the delayed guessing game using the strategy of randomly picking the next interval. If it picks
 * the correct one, it continues.
 */
class RandomBinaryGuesser(override val thinker: Thinker) : Guesser {

    private val random: Random = Random()



    override fun guess() {
        var max = thinker.thinkMax
        var min = thinker.thinkMin
        var mid = Util.calcMid(min, max)

        while (!thinker.isSolved) {
            println("Guessing $mid...")
            thinker.guess(mid)

            while (true) {
                // randomly go up or down
                val randomGuess: Int
                val randomResultValue: Thinker.ResultValue
                if (random.nextBoolean()) {
                    randomGuess = Util.calcMid(mid, max)
                    randomResultValue = Thinker.ResultValue.SMALLER
                } else {
                    randomGuess = Util.calcMid(min, mid)
                    randomResultValue = Thinker.ResultValue.GREATER
                }

                println("Guessing $randomGuess... (randomly going ${randomResultValue.name})")
                val result = thinker.guess(randomGuess)!!

                when (result.value) {
                    Thinker.ResultValue.CORRECT -> return
                    Thinker.ResultValue.SMALLER -> {
                        min = mid
                        mid = Util.calcMid(min, max)
                    }
                    Thinker.ResultValue.GREATER -> {
                        max = mid
                        mid = Util.calcMid(min, max)
                    }
                }

                if (randomResultValue != result.value) {
                    println("Incorrect random pick :(\n")
                    break
                }

                println("Correct random pick!\n")
            }
        }
    }

}